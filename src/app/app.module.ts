import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {BooksService} from './services/books.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatDialogModule, MatFormFieldModule, MatToolbarModule, MatTooltipModule} from '@angular/material';
import {MatListModule} from '@angular/material/list';
import {AddBookDialogComponent} from './components/add-book-dialog/add-book-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {UpdateBookDialogComponent} from './components/update-book-dialog/update-book-dialog.component';

@NgModule({
    declarations: [
        AppComponent,
        AddBookDialogComponent,
        UpdateBookDialogComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatListModule,
        MatDialogModule,
        HttpClientModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatToolbarModule,
        MatTooltipModule,
        MatInputModule,
        FormsModule
    ],
    providers: [BooksService],
    bootstrap: [AppComponent],
    entryComponents: [AddBookDialogComponent, UpdateBookDialogComponent]
})
export class AppModule {
}
