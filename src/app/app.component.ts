import {Component, DoCheck, OnDestroy} from '@angular/core';
import {BooksService} from './services/books.service';
import {Book} from './models/book';
import {MatDialog} from '@angular/material';
import {AddBookDialogComponent} from './components/add-book-dialog/add-book-dialog.component';
import {createBookMock} from './utils/mocks';
import {UpdateBookDialogComponent} from './components/update-book-dialog/update-book-dialog.component';
import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, DoCheck {
    books: Book[];
    destroyer$ = new Subject();
    generateCounter = 1000;
    endTime;
    startTime;
    removingAll = false;
    counter = 0;

    constructor(private service: BooksService, public dialog: MatDialog) {
        this.loadBooks();
    }

    ngDoCheck(): void {
        this.endTime = new Date();
        if (this.endTime && this.startTime) {
            this.counter++;
            if (this.counter === this.generateCounter && !this.removingAll) {
                console.log(`Czas wyrenderowania ${this.generateCounter} elementów: `,
                    this.calcTimeInMs(this.startTime, this.endTime) + 'ms');
            } else if (this.counter === (this.books.length - 1) && this.removingAll) {
                console.log(`Czas usunięcia wszystkich elementów z ekranu: `,
                    this.calcTimeInMs(this.startTime, this.endTime) + 'ms');
            }
        }
    }


    ngOnDestroy(): void {
        this.destroyer$.next(true);
        this.destroyer$.complete();
    }

    openDialogToCreateBook(): void {
        const dialogRef = this.dialog.open(AddBookDialogComponent, {
            width: '750px'
        });

        dialogRef.afterClosed().pipe(
            filter((res) => !!res),
            takeUntil(this.destroyer$)
        ).subscribe(result => this.createBook(createBookMock(result.title, result.price)));
    }

    openDialogToUpdateBook(book: Book): void {
        const dialogRef = this.dialog.open(UpdateBookDialogComponent, {
            width: '550px',
            data: book.title
        });

        dialogRef.afterClosed().pipe(
            filter((res) => !!res && !!res.title),
            takeUntil(this.destroyer$)
        ).subscribe(result => this.updateBook({...book, title: result.title}));
    }

    loadBooks() {
        this.service.loadBooks().pipe(
            takeUntil(this.destroyer$)
        ).subscribe(books => this.books = books);
    }

    createBook(book: Book) {
        this.service.addBook(book).pipe(
            takeUntil(this.destroyer$)
        ).subscribe();
    }

    updateBook(book: Book) {
        this.service.updateBook(book).pipe(
            takeUntil(this.destroyer$)
        ).subscribe();
        // now without refresh
    }

    removeBook(id: string) {
        this.service.removeBook(id).pipe(
            takeUntil(this.destroyer$)
        ).subscribe(() => this.books = this.books.filter(book => book._id !== id));
    }

    generateBooks() {
        this.initValues();
        const startTime = new Date();
        for (let i = 0; i < this.generateCounter; i++) {
            this.createBook(createBookMock(i.toString()));
        }
        this.loadBooks();
        const endTime = new Date();
        console.log('Czas operacji tworzenia ' + this.generateCounter + ' książek wynosi: ' + this.calcTimeInMs(startTime, endTime) + 'ms');
    }

    private calcTimeInMs(startTime: Date, stopTime: Date): number {
        return stopTime.getTime() - startTime.getTime();
    }

    removeAllBooks() {
        this.initValues();
        this.removingAll = true;
        const startTime = new Date();
        this.books.forEach(book => this.removeBook(book._id));
        const endTime = new Date();
        console.log('Czas operacji usuwania ' + this.books.length + ' książek wynosi: ' + this.calcTimeInMs(startTime, endTime) + 'ms');
    }

    updateAllBooks() {
        this.initValues();
        const startTime = new Date();

        this.books.forEach(book => this.updateBook({
            ...book,
            title: book.title + '- zedytowana'
        }));
        this.loadBooks();

        const endTime = new Date();
        console.log('Czas operacji aktaulizowania ' + this.books.length + ' książek wynosi: '
            + this.calcTimeInMs(startTime, endTime) + 'ms');
    }

    private initValues() {
        this.startTime = new Date();
        this.counter = 0;
        this.removingAll = false;
    }
}

