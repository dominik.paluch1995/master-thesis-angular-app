import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'app-update-book-dialog',
    templateUrl: './update-book-dialog.component.html',
    styleUrls: ['./update-book-dialog.component.scss']
})
export class UpdateBookDialogComponent {

    bookForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public title: string,
        public dialogRef: MatDialogRef<UpdateBookDialogComponent>) {
        this.bookForm = this.fb.group({
            title: ['', [Validators.required]]
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    titleIsCorrected(): boolean {
        return this.bookForm.get('title').value.trim() !== '';
    }

}
