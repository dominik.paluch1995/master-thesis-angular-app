import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';

@Component({
    selector: 'app-add-book-dialog',
    templateUrl: './add-book-dialog.component.html',
    styleUrls: ['./add-book-dialog.component.scss']
})
export class AddBookDialogComponent {

    titleControl;
    priceControl;
    textPattern = '([ ]*[A-Za-z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ_]+[ ]*)*';
    decimalPattern = '[0-9]+((\\.)[0-9]+)?';

    constructor(
        public dialogRef: MatDialogRef<AddBookDialogComponent>) {
        this.titleControl = new FormControl('', [Validators.required, Validators.pattern(this.textPattern)]);
        this.priceControl = new FormControl(0, [Validators.max(100), Validators.min(0)]);

    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    getErrorMessageForTitle() {
        return this.titleControl.touched && this.titleControl.hasError('required') ? 'Tytuł jest wymagany' :
            this.titleControl.hasError('pattern') ? 'Nie używaj znaków specjalnych, maksymalnie 30 znaków' : '';
    }

    getErrorMessageForPrice() {

        return this.priceControl.touched &&
        this.priceControl.hasError('max') ? 'Maksymalnie cena to 100 zł' : '';
    }
}
