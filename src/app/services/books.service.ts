import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Book} from '../models/book';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class BooksService {

    API_URL = environment.api_url;

    constructor(private http: HttpClient) {
    }

    loadBooks(): Observable<Book[]> {
        return this.http.get<Book[]>(`${this.API_URL}/api/books`);
    }

    addBook(book: Book): Observable<Book> {
        return this.http.post<Book>(`${this.API_URL}/api/books`, book);
    }

    updateBook(book: Book): Observable<Book> {
        return this.http.put<Book>(`${this.API_URL}/api/books/${book._id}`, book);
    }

    removeBook(id: string): Observable<any> {
        return this.http.delete(`${this.API_URL}/api/books/${id}`);
    }
}
