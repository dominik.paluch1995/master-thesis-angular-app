import {Book} from '../models/book';
import {Author} from '../models/author';
import {CategoryType} from '../models/category-type';

export function createBookMock(title?: string, price?: number): Book {
    return {
        title: !!title ? title : 'Tytuł',
        price: !!price ? price : 0,
        author: createAuthorMock(),
        category: CategoryType.ROMANCE,
        isbn: '978-3-16-148410-0',
        publishingHouse: 'Albatros',
        releaseDate: new Date()
    };
}

export function createAuthorMock(): Author {
    return {
        nationality: 'Polska',
        name: 'Adam',
        surname: 'Mickiewicz'
    };
}
